from django.shortcuts import render
from random import choice
# Create your views here.


def main(request):
    bpp = ('Baseform', 'Pastform', 'Pastparticiple')
    words = irregular_words()
    dict_words = dict()
    for i, j in zip(words, bpp):
        dict_words[j] = i
        dict_words[j] = i
        dict_words[j] = i

    return render(request, 'index.html', {'word': dict_words})

def irregular_words() -> list:
    with open('verbs/wordlist.txt', 'r') as words_list:
        word = choice(words_list.readlines())
        word = word.strip().split(':')
        return word
