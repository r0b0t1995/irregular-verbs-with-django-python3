from django.apps import AppConfig


class MainverbsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'verbs'
